<h1 align="center"> Hello everyone 👋 Welcome to my repository ! </h1>
<div>
  <h2> What was used ..? 🔌</h2>
  <ul>
    <li> <i> Vuejs </i> </li>
    <li> <i> JavaScript </i> </li>
    <li> <i> CSS </i> </li>
    <li> <i> Bootstrap </i> </li>
   <ul>

  <h2> Project Setup ⚒</h2>
     <strong> <p> npm install ⚒</p> </strong>
     <strong> <p> yarn install ⚒</p> </strong>
     
  <h2> Project RUN 🚀</h2>
     <strong> <p>npm run dev 🏌️‍</p> </strong>
     <strong> <p>yarn dev 🏌️‍</p> </strong>

  <h2> Project BUILD 🚀</h2>
     <strong> <p>npm run build 🚀</p> </strong>
</div>

####
<strong> Subscribe if you want to contribute 😉 </strong>
